"""
Time Complexity for adjacent function: O(1):

Because for each pair, it takes O(1) to check whether the first node is the key in the dictionary or not;
It takes another O(1) for the second node to insert into the value list for its parent as well.
So total time conplexity is O(1) for each pair.
"""

"""
# Time Complexity for identify_router function: O(n):

Because it takes O(n) to count the number of value for each key in the dictionary of dic 
and takes O(1) to put the key-value pair into a new dictionary called count;
It also takes another O(n) to find the max value in the dictionary of count.
So total time complexity is O(n).
"""

dic = {}

# function for connecting neighbors for each node using dictionary
def adjacent(pair):
    parent = pair[0]
    adj = pair[1]
    dic[parent] = dic.get(parent, [])
    dic[parent].append(adj)


# function for identify_router and counting the number of connection and finding the max 
def identify_router():
    if len(dic) == 0:
        return {}
    count = {}
    for key, val in dic.items():
        parent = key
        for adj in val:
            parent_val = count.get(parent, 0) + 1
            adj_val = count.get(adj, 0) + 1
            count[parent] = parent_val
            count[adj] = adj_val

    max = 0 # record the max number
    node = [] # record the node that has the max number

    # finding the max number
    for key, val in count.items():
        if max < val:
            node = []
            node.append(key)
            max = val
        elif val == max:
            node.append(key)
    return node


adjacent([2,4])
adjacent([4,6])
adjacent([6,2])
adjacent([2,5])
adjacent([5,6])

print(dic)
print(identify_router())