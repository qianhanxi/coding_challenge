"""
Time Complexity: O(n)

Because it takes O(n) to change the string to array first. 
Then it takes O(n) to traverse from the left to the right by using two pointers to count the number of same character.
Then it takes O(n) to change the array to string.
So total time complexity is O(n)
"""

def compress(input):
    chars = [i for i in input]
    if not chars or len(chars) == 1:
        return input
    left, right = 0, 0
    res = 0
    count = 0
    while left < len(chars):
        while right < len(chars) and chars[left] == chars[right]:
            right += 1
        count = right - left
        chars[res] = chars[left]
        res += 1
        if count > 1:
            while count > 0:
                tmp = str(count)
                for i in range(len(tmp) -1, -1, -1):
                    ch = str(count // 10**i)
                    chars[res] = ch
                    count %= 10**i
                    res += 1
                    # print(count)
        left = right
    newchar = ''
    for i in range(res):
        newchar += chars[i]
    return newchar

test1 = 'bbcceeee'
output1 = compress(test1)
print('Result for test1 is: ' + output1)

test2 = 'aaabbbcccaaa'
output2 = compress(test2)
print('Result for test2 is: ' + output2)

test3 = 'a'
output3 = compress(test3)
print('Result for test3 is: ' + output3)